# SPDX-License-Identifier: GPL-2.0+

# Copyright © 2022 Collabora Ltd
# Copyright © 2022 Valve Corporation

. ${LIBDIR:-.}/steamos-repair.sh

declare -A part_uuid=()
declare -A part_dev=()

# ============================================================================
# misc utility:

abort_restore ()
{
    local -i status=${1:-}

    log_msg "$*"
    set_session_status $status
    exit 0
}

# Get a block device for a partition uuid, guaranteeing
# that said device is on the parent device we specify:
find_part_dev ()
{
    local -r dev=${1:-}
    local -r uuid=${2:-}
    local found=
    local parent part_dev part_uuid

    while read parent part_dev part_uuid
    do
        [ "$parent"    = "$dev"  ] || continue
        [ "$part_uuid" = "$uuid" ] || continue
        found=$part_dev
        break
    done < <(lsblk -n -r -o pkname,name,partuuid)

    [ -n "$found" ] && echo $found
}

# ============================================================================
# arg validation:

# barf if the specified device is not a block device:
check_disc_target ()
{
    local disc=${1:-}
    local -i status=0

    if [ -z "$disc" ]
    then
        emit_json_response 400 \
                           "Repair requires a valid device target (/dev/$disc)"
        exit 0
    fi

    if [ ! -b "/dev/$disc" ]
    then
        if [ -e "/dev/$disc" ]
        then
            status=422
        else
            status=404
        fi

        emit_json_response $status \
                           "Target device /dev/$disc is not a block device"
        exit 0
    fi
}

# ============================================================================
# extract data from a given scan session:

# Extract a valid UUID from the harvested partset data from a scan session
# this tries to handle different partset collections having conflicting
# ideas for what the part uuid of a given label (eg rootfs-A) should be.
# The uuid with the most votes wins (this is not guaranteed to be correct
# but in the [unlikely?] event of a conflict we have to decide _something_)
validated_uuid ()
{
    local label=${1:-}
    local disc=${2:-}
    local pset=${3:-}
    local scan=${4:-}
    local uuid_list=$(get_data ${label}.${disc}.${pset} partuuid $scan)
    local uuid=
    local -A seen=()
    local x=
    local validated=

    for uuid in $uuid_list
    do
        x=${seen[$uuid]:-0}
        seen[$uuid]=$((x + 1))
    done

    for uuid in "${!seen[@]}"
    do
        if [ ${seen[$uuid]:-0} -gt $x ] && [ "$uuid" != UNKNOWN ]
        then
             x=${seen[$uuid]}
             validated="$uuid"
        fi
    done

    echo "$validated"
}

# load label -> partition uuid data into part_uuid for { disc, image, scan }
# where scan is the session uuid we are using as our data source:
load_part_uuids ()
{
    local -r disc=${1:-}
    local -r scan=${2:-}
    local images=${3:-}
    local -r partsets=$(list_data_by_type $disc.partset $scan)
    local pset=
    local what=
    local i=

    if [ ! "$images" ]
    then
        images=$(list_data_by_type $disc.partset $scan)
    fi

    for i in $images
    do
        part_uuid[esp]=$(validated_uuid esp $disc $i $scan)
        part_uuid[home]=$(validated_uuid home $disc $i $scan)
    done

    for pset in $partsets
    do
        for what in efi rootfs var
        do
            part_uuid[$what-$pset]=$(validated_uuid $what $disc $pset $scan)
        done
    done
}

# Map the partition uuids from part_uuid to block devices, restricting
# ourselves to candidates on the specified parent disc block device.
# Load said info into part_dev:
load_part_devs ()
{
    local disc=${1:-}
    local partition=
    local parent=
    local device=

    for partition in "${!part_uuid[@]}"
    do
        device=$(find_part_dev $disc ${part_uuid[$partition]})
        part_dev[$partition]=$device
    done
}

# ============================================================================
# restoring/resetting OS image partitions:

# ----------------------------------------------------------------------------
# rootfs partitions:

# Reset the rootfs for image img from src, setting its fs uuid to new_uuid
# and giving it the correct label:
reset_root ()
{
    local -r src=${1:-}
    local -r img=${2:-}
    local -r new_uuid=${3:-}
    local -r dst=${part_dev[rootfs-$img]:-}
    local -r label=rootfs-$img
    local fsinfo=
    local dd_out=

    [ -b "/dev/$dst" ] || \
        abort_restore 404 "rootfs device '$dst' for image '$img' not found"

    if [ -e "$src" ]
    then
        fsinfo=$(file -Lsb "$src" 2>/dev/null)
    else
        abort_restore 404 \
                      "Root fs source '${src:-}' for image ${img:-} not found"
    fi

    log_msg "Restoring rootfs-$img on /dev/$dst from $src"
    dd_out=$(dd if="$src" iflag=fullblock bs=16M oflag=direct of=/dev/$dst 2>&1)
    log_msg "$dd_out"

    case ${fsinfo} in
        (*BTRFS*)
            log_msg "Carrying out btrfs check on /dev/$dst ($label)"
            btrfs check /dev/$dst
            log_msg "Setting fs uuid for $label to to $new_uuid"
            btrfstune -U "$new_uuid" /dev/$dst
            btrfs filesystem label /dev/$dst "$label"
            ;;
        (*ext4*)
            log_msg "Carrying out ext fsck on /dev/$dst ($label)"
            e2fsck -f -y /dev/$dst
            log_msg "Setting fs uuid for $label to to $new_uuid"
            tune2fs -L "$label" -U "$new_uuid" /dev/$dst
            ;;
    esac
}

# ----------------------------------------------------------------------------
# ESP partition

# need to know where both the target esp and efi fs are mounted for this:
reset_bootconf ()
{
    local -r espmount=${1:-}
    local -r efimount=${2:-}
    local -r confdir=${espmount}/SteamOS/conf
    local -r img=${3:-}

    mkdir -p "$confdir" ||
        abort_restore 500 "Unable to create bootconf directory '$confdir'"

    # don't worry if this fails, we'll either recover or catch the error
    # on the next call, depending on how new out steamos-bootconf is:
    steamos-bootconf \
        --conf-dir "$confdir" --efi-dir "$efimount" --image ${img} create ||
        true

    log_msg "Initialising boot config at $confdir/$img.conf"
    steamos-bootconf \
        --conf-dir "$confdir" --efi-dir "$efimount" --image ${img} \
        set-mode reboot ||
        abort_restore 500 "Could not initialise boot config for '$img'"
}

mount_esp ()
{
    local -r espmount=${1:-$(mktemp -d)}
    local -r esp=${part_dev[esp]:-}

    log_msg "Mounting target ESP /dev/$esp at $espmount"
    mount "/dev/$esp" "$espmount" ||
        abort_restore 500 "Mount esp '$esp' at '$espmount' failed: $?"
    push_exit_trap "sleep 3; umount -f $espmount"
}

unmount_esp ()
{
    local -r espmount=${1:-}

    log_msg "Unmounting target ESP from $espmount"
    sleep 3
    umount -f "$espmount" && rmdir "$espmount"
    pop_exit_trap
}

reset_esp ()
{
    local -r espmount=${1:-}
    local -r efimount=${2:-}
    local -r img=${3:-}
    local -r cl_src=/usr/lib/steamos-efi/x86_64-efi/steamcl.efi
    local -r cl_ver=/usr/share/steamos-efi/steamcl-version
    local cl_dest=
    local cl_dir=
    local cl_flag=

    for cl_dest in $espmount/efi/steamos/steamcl.efi \
                   $espmount/efi/boot/bootx64.efi
    do
        if atomic_sync "$cl_src" "$cl_dest"
        then
            log_msg "Copied $cl_src to $cl_dest"
            cl_dir=$(dirname "$cl_dest")

            # copy the steamcl-version file, but only to the steamos
            # specific loader location:
            if [ "$cl_dir" = "$espmount/efi/steamos" ]
            then
                atomic_sync "$cl_ver" "$cl_dir"/$(basename "$cl_ver")
                rm -f "$cl_dir"/factory-reset/*
            fi

            # remove any chainloader flag files
            # add restricted back in as we want that one:
            while read cl_flag
            do
                case $cl_flag in
                    */steamcl-restricted|*/steamcl-version)
                        continue
                        ;;
                esac

                log_msg "Clearing $cl_flag"
                rm -f "$cl_flag"

            done < <(ls -1 "$cl_dir"/steamcl-*)
            touch "$cl_dir/steamcl-restricted"
        else
            log_msg "Copied $cl_src to $cl_dest failed: $?"
        fi
    done

    reset_bootconf "$espmount" "$efimount" "$img"
}

# ----------------------------------------------------------------------------
# EFI partitions

restore_efi_bootloader ()
{
    local -r efi=${1:-}
    local -r uuid=${2:-}
    local -r loader_cfg="$efi/EFI/SteamOS/grub.cfg"
    local -r cfg_template="$PKGDATA/grub.cfg.template"
    local -r loader_ii=EFI/SteamOS/grubx64.efi

    [ -f $cfg_template ] ||
        abort_restore 404 "Missing bootloader config template $cfg_template"

    mkdir -p $(dirname $loader_cfg)
    sed -re "s/@ROOTFS_UUID@/${uuid}/g" "$cfg_template" > $loader_cfg.new

    [ $? -eq 0 ] ||
        abort_restore 500 \
                      "Create $loader_cfg.new with fs uuid '$uuid' failed: $?"

    mv "$loader_cfg.new" "$loader_cfg" ||
        abort_restore 500 "Restore loader config at $loader_cfg failed: $?"

    atomic_copy "$STAGE2_EFI" "$efi/$loader_ii" ||
        abort_restore \
            500 "Copy from $STAGE2_EFI to $efi/$loader_ii failed: $?"

}

restore_partset_info ()
{
    local -r wdir=${1:-}/SteamOS/partsets
    local -r img=${2:-}
    local pset=
    local target=
    local what=
    local efi_uuid=
    local root_uuid=
    local var_uuid=
    local home_uuid=
    local esp_uuid=
    local cmesg=
    local mmesg=
    local missing=
    local want=

    # We _must_ have the shared partset info, and the partset info
    # for our target image. The other images are nice to have - we'd
    # like them, but it's perfectly survivable not to have them

    log_msg "Creating partset info dir $wdir"
    mkdir -p "$wdir" ||
        abort_restore 500 "Failed to restore partset directory at $wdir"

    for pset in A B dev
    do
        target=$wdir/$pset
        efi_uuid=${part_uuid[efi-$pset]:-}
        root_uuid=${part_uuid[rootfs-$pset]:-}
        var_uuid=${part_uuid[var-$pset]:-}
        if [ "$efi_uuid" ] && [ "$root_uuid" ] && [ "$var_uuid" ]
        then
            log_msg "Creating partset file $target"
            cat - > "$target.new" <<EOF
efi $efi_uuid
rootfs $root_uuid
var $var_uuid
EOF
            if [ $? -eq 0 ] && mv "$target.new" "$target"
            then
                continue
            fi

            log_msg "Failed to create $target ($?)"
        fi
    done

    if [ -f "$wdir/$img" ]
    then
        if ! cp "$wdir/$img" "$wdir/self"
        then
            log_msg "Failed to create $wdir/self from $wdir/$img"
        fi
    fi

    case $img in
        A)
            cp "$wdir/B" "$wdir/other"
            ;;
        B)
            cp "$wdir/A" "$wdir/other"
            ;;
    esac

    home_uuid=${part_uuid[home]:-}
    esp_uuid=${part_uuid[esp]:-}

    if [ "$home_uuid" ] && [ "$esp_uuid" ]
    then
        target="$wdir/shared"
        cat - > "$target.new" <<EOF
esp $esp_uuid
home $home_uuid
EOF
        [ $? -eq 0 ] && mv "$target.new" "$target"
    fi

    target="$wdir/all"
    echo > "$target.new"
    for what in esp home {rootfs,efi,var}-{A,B,dev}
    do
        if [ "${part_uuid[$what]:-}" ]
        then
            echo "$what" "${part_uuid[$what]}" >> "$target.new"
        fi
    done

    mv "$target.new" "$target"

    for pset in A B dev shared all self other
    do
        if [ -f "$wdir/$pset"  ]
        then
            cmesg=${cmesg}${cmesg:+ }${pset}
        else
            mmesg=${mmesg}${mmesg:+ }${pset}
        fi
    done

    log_msg "Created partsets: ${cmesg:--}"
    log_msg "Missing partsets: ${mmesg:--}"

    missing=0
    mmesg=
    want=
    for pset in "$img" self shared all
    do
        want=${want}${want:+ }${pset}
        [ -f "$wdir/$pset" ] && continue
        missing=1
        mmesg=${mmesg}${mmesg:+ }$pset
    done

    if [ $missing -ne 0 ]
    then
        abort_restore \
            500 "Partsets: want {$want}; have {$cmesg}; missing {$mmesg}"
    fi
}

# ----------------------------------------------------------------------------
# EFI & ESP combined (bootconf reset requires both to be mounted)

reset_efi_and_esp ()
{
    local -r img=${1:-}
    local -r root_fsuuid=${2:-}
    local -r espmount=${3:-}
    local -r efi_dev=${part_dev[efi-$img]:-}
    local -r root_dev=${part_dev[rootfs-$img]:-}
    local -r efilabel=EFI-$img
    local -r efimount=$(mktemp -d)

    [ -b /dev/$efi_dev ] ||
        abort_restore 404 "No efi device '$efi_dev' for img '$img'"

    uuid_ok "$root_fsuuid" ||
        abort_restore 400 "Bad rootfs-$img uuid '$root_fsuuid' from '$root_dev'"

    log_msg "Creating EFI fs img '$efilabel' on /dev/$efi_dev"
    mkfs.vfat -n "$efilabel" /dev/$efi_dev ||
        abort_restore 500 "mkfs.vfat on $efi_dev failed"

    log_msg "Mounting EFI fs at $efimount"
    mount /dev/$efi_dev $efimount ||
        abort_restore 500 "mount of $efi_dev on $efimount failed"
    push_exit_trap "sleep 3; umount -f $efimount"

    log_msg "Restoring partset information for $efilabel"
    restore_partset_info $efimount $img

    log_msg "Restoring stage II bootloader config on $efilabel ($root_fsuuid)"
    restore_efi_bootloader $efimount $root_fsuuid

    # we want the target efidir mounted while this runs:
    reset_esp $espmount $efimount $img

    log_msg  "Unmounting $efilabel from $efimount"

    # prob harmless if this fails: randomly generated mount point
    # and will go away without trace on reboot:
    sleep 3
    umount -f $efimount && rmdir $efimount
    pop_exit_trap
}


# ----------------------------------------------------------------------------
# ext4 partitions
reset_ext4 ()
{
    local device=${1:-}
    local label=${2:-}
    local -a fs_opts=()
    local -a opts=(-qF)
    local features=
    local tmp=
    local mt_point=
    local mt_opts=

    # can't have the device mounted while we reformat it
    # but we do want to save the mount opts if it is mounted
    local proc_dev proc_mnt proc_fs proc_opts proc_etc
    while read proc_dev proc_mnt proc_fs proc_opts proc_etc
    do
        if [ "/dev/$device" != "$proc_dev" ]; then
            continue
        fi
        mt_point="$proc_mnt"
        mt_opts="$proc_opts"
        warn "Unmounting $device from $mt_point ($mt_opts)"
        umount -f "/dev/$device"
    done < /proc/mounts

    if findmnt "/dev/$device" >/dev/null 2>&1
    then
        abort_restore 500 "Unable to unmount /dev/$device ($label)"
        exit 0
    fi

    if [ "$label" ]
    then
        opts+=(-L "$label")
        if [ "$label" = "home" ]
        then
            opts+=(-m 0)
        fi
    fi

    # use cached opts from FACTORY_RESET_CONFIG_DIR, alternatively read them
    # from the filesystem
    read -r -a fs_opts < <(tune2fs -l "/dev/$device" | \
                           sed -n 's/^Filesystem features:\s*//p')

    # copy the important fs opts explicitly (currently just casefold):
    for tmp in "${fs_opts[@]}";
    do
        if [ "$tmp" = "casefold" ]; then
            opts+=(-O casefold)
            break
        fi
    done

    log_msg "Making ext4 filesystem on /dev/$device (options: ${opts[*]})"

    mkfs.ext4 "${opts[@]}" "/dev/$device"

    if [ -n "$mt_point" ];
    then
        warn "Remounting fresh fs on $device at $mt_point ($mt_opts)"
        mount ${mt_opts:+-o} $mt_opts "$device" "$mt_point"
    fi
}


# ----------------------------------------------------------------------------
# var partitions

reset_var ()
{
    local -r img=${1:-}
    local -r var_dev=${part_dev[var-$img]:-}

    [ -b /dev/$var_dev ] ||
        abort_restore 404 "Image $img has no var block device ($var_dev)"

    reset_ext4 $var_dev "var-$img"
}

# ----------------------------------------------------------------------------
# home parttion

reset_home ()
{
    local -r home_dev=${part_dev[home]:-}

    [ -b /dev/$home_dev ] ||
        abort_restore 404 "Image $img has no home block device ($home_dev)"

    reset_ext4 $home_dev \"home\"
}
