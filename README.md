# steamos-repair-backend

Backend web services for steamos-repair tool

## lighttpd Integration

A lighttpd config file called "steamos-repair.conf" wil be generated
and installed in /etc/lighttpd/.

You can replace the regular /etc/lighttpd/lighttpd.conf file with
this (or symlink the default location to it) if you have no other
requirements for lighttpd.

## setup

The restore service needs the following data sources that are not
part of this package - they must be added to the OS image the
service is running on:

  * /srv/steamos/rootfs.img.
    * A vanilla rootfs image for SteamOS
  * /srv/steamos/grubx64.efi
    * An x86-64 grub efi executable suitable for use as a stage ii loader
    * This can be lifted directly from a stock SteamOS 3 image

I've been testing with a known-good rootfs image dd'd out of a working image
to that location, and a grubx64,efi copied from the same SteamOS instance.

## Build dependencies

  * autoconf
  * automake
  * gcc
  * make

## Install dependencies

  * lighttpd
  * coreutils
  * util-linux
  * bash
  * mount
  * file
  * btrfs
  * btrfstune
  * e2fsck
  * tune2fs
  * sed
  * steamos-bootconf

## Transport details

  * Services:
    * will be accessible over HTTP 
    * will be avilable at address 127.0.0.1 (ie on the loopback interface)
    * will be available on port 8080
    * will respond with application/json payloads

### Debugging

NOTE: the wrapper program handles turning the query string into command line
arguments of the form name=value passed via execv(3), so they are not subject
to shell globbing, word splitting, and all that stuff.

This has the added advantage that you can run the commands from the source
tree:

    autoreconf -ivf # needed first time only
    ./configure --prefix=/usr --libexecdir=/usr/lib
    make

    sudo ./scan
    ./status
    ./status uuid=deadbeef-abad-1dea-1337-d155a715f1ed
    ./list   uuid=deadbeef-abad-1dea-1337-d155a715f1ed
    ./restore disc=nvme0n1 scanuuid=deadbeef-abad-1dea-1337-d155a715f1ed image=A

NOTE: lighttpd runs with an isolated /tmp, so you can't share results/sessions
between the web service and debug sessions run from a terminal.

NOTE: When run from a terminal, stderr isn't redirected, so you'll see
any stderr output.

## Service response

The response to each service request will be a JSON payload.
It will always contain the following elements:

  * service: (string) the name of the service
  * version: (string) x.y.z style service version
  * status: (integer) the status of the response
    * These will follow the http error codes, so:
      * 200 = success
      * 102 = long running process started
      * 400 and up = some sort of error
    * This is distinct from the HTTP status of the _transport_
  * message: (string) a short description, eg "reset initiated"

In addition if the status is 100-199:

  * uuid: (string) a uuid to identify this request
    * eg: deadbeef-abad-1dea-1337-d155a715f1ed
      * used by long running requestS to identify log messages relating to them

## Services

These are avaiulable at the following URLs

### /scan

Triggers a back-end scan of visible discs to identify possible SteamOS/holo
images that can be restored/reset/repaired/rebooted etc.

Scans can be slow so this will return a UUID which can be used to fetch the
scan results when they are ready, via the /list service.

Its status will typically be 102

### /list?uuid=`UUID`

Returns a standard response, plus a data structure detailing the available
SteamOS images. It will _exclude_ the OS image the repair tool is running on.

    {"service":"list",
     "version":"0.1",
     "status":200,
     "message":"SteamOS Installation List",
     "uuid":"deadbeef-abad-1dea-1337-d155a715f1ed",
     "os-list":{"nvme0n1":
                {"disc":"KINGSTON SA2000M8250G 232.9G",
                 "images":{"A":{"esp":"d262de25-6ebe-be4d-a09b-7cc0be7da572",
                                "efi":"64378c8e-8c94-e347-a52f-f5cd9b7aa9c5",
                                "rootfs":"ef2eeae5-5892-294a-9a67-5863d78c907f",
                                "var":"729f3fbe-ad1c-ac43-9cea-18d164dd5da3",
                                "home":"759038ba-dcbc-2345-b318-72b94c147772"},
                           "B":{"esp":"d262de25-6ebe-be4d-a09b-7cc0be7da572",
                                "efi":"8623e7b8-e782-5d43-b957-d33db872dd34",
                                "rootfs":"207532b4-87fd-2b4d-906c-277980bef3c4",
                                "var":"bc275f64-2ad3-9a45-90f6-881674bb30c3",
                                "home":"759038ba-dcbc-2345-b318-72b94c147772"}} }}
    }

### /status

Returns the current status of all long running repair requests

    {"service":"list",
     "version":"0.1",
     "status":200,
     "message":"SteamOS Repair Service Request Statuses",
     "uuid":"deadbeef-abad-1dea-1337-d155a715f1ed",
     "status-list":
       {"49bff5d3-f8ff-47e8-9926-4c52bb13db82": [102, "Repairing nvme0…", <PID>],
        "7ada59b6-0818-42d9-b400-674f5ca0ac14": [200, "Scan completed", 0]}}

### /status?uuid=`UUID`;start=23;max=20

Returns status and log messages for a single request, identified by `UUID`:
If `start` is specified, starts at that log message number.
If `max` is specified, returns no more than that many messages.

    {"service":"list",
     "version":"0.1",
     "status":200,
     "message":"SteamOS Repair Service Request Statuses",
     "uuid":"deadbeef-abad-1dea-1337-d155a715f1ed",
     "status-list":
       {"49bff5d3-f8ff-47e8-9926-4c52bb13db82": [102, "Scanning...", <PID>]}
     "log-messages":
       ["Starting scan",
        "Scanning efi candidate d262de25-6ebe-be4d-a09b-7cc0be7da572"]}

### /restore?disc=`device`;image=`X`;scanuuid=`UUID`

Begins a long running repair process on disc `device` for the
partset identified by image ident `X`, based on the results of
the scan with uuid `UUID`.

Does the following:

  - based on disc, scan-uuid, image-label from previous /scan session
    - replace the rootfs
      - dd a pre-existing rootfs to the root-X device
      - randomise the FSUUID of the target device

  - replace the EFI fs
    - reformat the EFI device for the target image
    - relabel EFI device

  - restore the stage II bootloader
    - /efi/EFI/SteamOS/grubx64.efi (copied from running OS)
    - /efi/EFI/SteamOS/grub.cfg (generated from template with FSUUID from above)

  - recreate partset files:
    - /efi/SteamOS/partsets/shared
    - /efi/SteamOS/partsets/all
    - /efi/SteamOS/partsets/A
    - /efi/SteamOS/partsets/B
    - /efi/SteamOS/partsets/dev
    - /efi/SteamOS/partsets/self
    - *OPTIONAL* /efi/SteamOS/partsets/other
      - this one doesn't get created for `dev` images

  - replace the chainloader in /esp
    - /esp/efi/steamos/steamcl.efi
    - /esp/efi/steamos/steamcl-version
    - /esp/efi/steamos/factory-reset
    - /esp/efi/steamos/steamcl-restricted
    - /esp/efi/boot/bootx64.efi
    - /esp/efi/boot/steamcl-restricted

  - update boot configurations to prefer the newly-installed partition.
  - this should not impact the home partition.
  - prompt for reboot when complete
    - /esp/SteamOS/conf/{A,B,dev}.conf

  - wipe /var
    - gets replaced by dracut on boot

### /factory-reset?disc=`device`;scanuuid=`UUID`

Begins a long running factory reset process on disc <device>
based on scanuuid `UUID`.

 - performs the same steps as /restore
   - for each detected image
 - also scrubs and recreates the home fs

### /prepare-chroot?disc=`device`;image=`X`;scanuuid=`UUID`

Assembles a chroot-ready file system tree based on the partset
in question, with the following trees bind-mounted in:

  - /dev
  - /proc
  - /run
  - /sys
  - /sys/firmware/efi/efivars
  - /tmp
    - *not* shared with host OS

### /reboot?disc=`device`;image=`X`;scanuuid=`UUID`;delay=`N`

Initiates a reboot into image <X>.

  * delay: [optional] number of seconds to delay before rebooting
    * default 10
  * image: the image to boot into
  * scanuuid: the scan uuid to get the SteamOS metadata from
  * disc: the device on which the target oimage is to be found

Probably want to prompt the user that the USB stick nay need to be 
removed for this to complete successfully.

## /clear?uuid=<UUID>

Erase the data and log messages from session UUID from the cache
