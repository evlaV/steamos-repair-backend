#!/bin/bash

# SPDX-License-Identifier: GPL-2.0+

# Copyright © 2022 Collabora Ltd
# Copyright © 2022 Valve Corporation

LIBDIR=$(dirname "$0")

. ${LIBDIR:-.}/steamos-repair.sh

set -u

declare -i delay=10
declare disc=
declare image=
declare scan=
declare -A target=()

parse_args ()
{
    for arg in "$@"
    do
        case $arg in
            disc=*)
                disc=${arg#*=}
                ;;
            scanuuid=*)
                scan=${arg#*=}
                ;;
            image=*)
                image=${arg#*=}
                ;;
        esac
    done
}

validated_uuid ()
{
    local label=${1:-}
    local disc=${2:-}
    local pset=${3:-}
    local scan=${4:-}
    local uuid_list=$(get_data ${label}.${disc}.${pset} partuuid $scan)
    local uuid=
    local -A seen=()
    local x=
    local validated=

    for uuid in $uuid_list
    do
        x=${seen[$uuid]:-0}
        seen[$uuid]=$((x + 1))
    done

    for uuid in "${!seen[@]}"
    do
        if [ ${seen[$uuid]:-0} -gt $x ] && [ "$uuid" != UNKNOWN ] 
        then
             x=${seen[$uuid]}
             validated="$uuid"
        fi
    done

    echo "$validated"
}

find_part_devices ()
{
    local -r dev=${1:-}
    local -r img=${2:-}
    local -r uid=${3:-}
    local -r partsets=$(list_data_by_type $disc.partset $scan)
    local esp_uuid=$(validated_uuid esp $dev $img $uid)
    local efi_uuid=$(validated_uuid efi $dev $img $uid)

    if [ ! -b /dev/$dev ]
    then
        emit_json_response 404 "Device $dev not found"
        exit 0
    fi
    
    while read parent part_dev part_uuid
    do
        [ "${target[esp]:-}" ] && [ "${target[efi]:-}" ] && break
        [ "$parent" = "$dev" ] || continue

        if [ "$part_uuid" = "$esp_uuid" ]
        then
            target[esp]="$part_dev"
            continue
        fi

        if [ "$part_uuid" = "$efi_uuid" ]
        then
            target[efi]="$part_dev"
            continue
        fi
    done < <(lsblk -n -r -o pkname,name,partuuid)

    if [ ! "${target[esp]:-}" ] || [ ! "${target[efi]:-}" ]
    then
        emit_json_response \
            404 \
            "Need $dev esp, efi-$img: Have '${target[esp]:-}', '${target[efi]:-}'"
        exit 0
    fi
}

setup_bootconf ()
{
    local img=${1:-}
    local part=
    local mnt_esp=
    local mnt_efi=
    local bc_cmd=steamos-bootconf
    local -i img_ok=0
    local img_chosen=

    mnt_efi=$(mktemp -d)
    if mount /dev/${target[efi]} $mnt_efi
    then
        push_exit_trap "sleep 3; umount -f $mnt_efi; rmdir $mnt_efi"
        log_msg "EFI $img mounted at $mnt_efi"
        bc_cmd+=" --efi-dir $mnt_efi"
    else
        emit_json_response 500 \
                           "Could not mount EFI ${target[efi]} at $mnt_efi"
        exit 0
    fi

    mnt_esp=$(mktemp -d)
    if mount /dev/${target[esp]} $mnt_esp
    then
        push_exit_trap "sleep 3; umount -f $mnt_esp; rmdir $mnt_esp"
        log_msg "EFI $img mounted at $mnt_esp"
        bc_cmd+=" --conf-dir $mnt_esp/SteamOS/conf"
    else
        emit_json_response 500 \
                           "Could not mount ESP ${target[esp]} at $mnt_esp"
        exit 0
    fi

    local state label selected
    while read state label selected
    do
        [ "$label" = "$img" ] && img_ok=1
        [ "$selected" = "*" ] && img_chosen=$label
    done < <($bc_cmd list-images)

    log_msg "Image $img exists: $img_ok"
    log_msg "Chosen image is $img_chosen, want $img"

    if [ $img_ok -ne 1 ]
    then
        emit_json_response 400 "No such image $img exists in boot config"
        exit 0
    fi

    log_msg "Requesting next boot for $img (& clearing flags)"
    if $bc_cmd --image $img \
               set-mode reboot \
               --set image-invalid 0 \
               --set boot-other 0 \
               --set update 0
    then
        set_session_status 200
    else
        set_session_status 500
        log_msg "$bc_cmd --image $img set-mode reboot … failed"
    fi
    
    sleep 3
    umount -f $mnt_esp && pop_exit_trap
    umount -f $mnt_efi && pop_exit_trap
}

delayed_reboot ()
{
    local -i pause=${1:-}

    sleep $pause
    shutdown -r now
}

parse_args "$@"
set_uuid
set_session_status 102
set_session_type reboot

find_part_devices "$disc" "$image" "$scan"
setup_bootconf "$image"

declare -i final_status=$(get_session_status)

if [ $final_status -eq 200 ]
then
    (close_stdio; delayed_reboot $delay) &
fi

emit_json_response $final_status "Issuing shutdown in $delay seconds"
exit 0
